﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TechTalk.SpecFlow;

namespace TestInternalForCIWithSpecflowRun.Test
{
    [Binding]
    public class AddTestSteps
    {
        private int a,b;
        private int result;

        [Given(@"I have entered (.*) into the calculator")]
        public void GivenIHaveEnteredIntoTheCalculator(int p0)
        {
            a = p0;
        }
        
        [When(@"I press add")]
        public void WhenIPressAdd()
        {
            var target = new Calculator();
            result = target.InternalAdd(a, a);
        }
        
        [Then(@"the result should be (.*) on the screen")]
        public void ThenTheResultShouldBeOnTheScreen(int p0)
        {
            Assert.AreEqual(result, p0);
        }
    }
}
